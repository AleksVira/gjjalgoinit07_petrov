package com.getjavajob.training.algo.init.petrova;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class TaskCh02N043Test {

    @Test
    public void testSolution() throws Exception {
        TaskCh02N043.NumersAB testingNumbers = new TaskCh02N043.NumersAB();
        int expected;
        int actual;

        testingNumbers.a = 2;
        testingNumbers.b = 5;
        expected = 0;
        actual = TaskCh02N043.solution(testingNumbers);
        Assert.assertEquals(expected, actual);

        testingNumbers.a = 17;
        testingNumbers.b = 3;
        expected = 0;
        actual = TaskCh02N043.solution(testingNumbers);
        Assert.assertEquals(expected, actual);

        testingNumbers.a = 15;
        testingNumbers.b = 3;
        expected = 1;
        actual = TaskCh02N043.solution(testingNumbers);
        Assert.assertEquals(expected, actual);
    }
}