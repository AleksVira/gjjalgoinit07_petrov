package com.getjavajob.training.algo.init.petrova;

/**
 * Этот файл содержит решение задачи 3.29 из задачника
 * а) x % 2 != 0 && y % 2 != 0
 * б) result = x < 20 ^ y < 20
 * в) boolean result = x == 0 | y == 0
 * г) x < 0 & y < 0 & z < 0
 * д) (x % 5 == 0 ^ y % 5 == 0 ^ z % 5 == 0) & !(x % 5 == 0 & y % 5 == 0 & z % 5 == 0)
 * е) x > 100 | y > 100 | z > 100
 *
 * @author Petrov Aleksey
 * @version 1.0
 *
 */


public class TaskCh03N029 {
    public static void main(String[] args) {
        System.out.println("А ничего не делаем, смотрим комментарии внутри кода");
    }
}

