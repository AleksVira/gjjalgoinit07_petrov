package com.getjavajob.training.algo.init.petrova;

import java.util.Scanner;

import static java.lang.Math.sqrt;

public class TaskCh01N017r {
    public static void main(String[] args) {
        int input = consoleInput();
        double result = solution(input);
        consoleOutput(result);
    }

    private static double solution(int input) {
        double expression = (sqrt(input + 1) + sqrt(input - 1)) / (2 * sqrt(input));
        return expression;
    }


    private static int consoleInput() {
        Scanner inputScanner = new Scanner(System.in);
        System.out.print("Введите число --> ");
        int getNumber = inputScanner.nextInt();
        System.out.println("Вы ввели число " + getNumber);
        return getNumber;
    }

    private static void consoleOutput(double result) {
        System.out.println("Результат: " + result);
    }
}
