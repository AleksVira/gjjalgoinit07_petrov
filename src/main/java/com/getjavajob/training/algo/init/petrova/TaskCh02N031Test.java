package com.getjavajob.training.algo.init.petrova;

import org.junit.Assert;

public class TaskCh02N031Test {
    public static void main(String[] args) {
        test01();
        test02();
        test03();
        test04();
    }

    private static void test01() {
        Assert.assertEquals(100, TaskCh02N031.solution(100));
    }

    private static void test02() {
        Assert.assertEquals(132, TaskCh02N031.solution(123));
    }

    private static void test03() {
        Assert.assertEquals(878, TaskCh02N031.solution(887));
    }

    private static void test04() {
        Assert.assertEquals(999, TaskCh02N031.solution(999));
    }
}