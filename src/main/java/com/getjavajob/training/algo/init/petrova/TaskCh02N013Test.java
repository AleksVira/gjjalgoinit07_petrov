package com.getjavajob.training.algo.init.petrova;

import org.junit.Assert;

public class TaskCh02N013Test {
    public static void main(String[] args) {
        test01();
        test02();
//        test03();
    }

    private static void test01() {
        Assert.assertEquals(321, TaskCh02N013.solution(123));
    }

    private static void test02() {
        Assert.assertEquals(105, TaskCh02N013.solution(501));
    }

    //    Поскольку с числами как со строками по условию работать нельзя, то тест на ноль в третьей позиции не проходит
    private static void test03() {
        Assert.assertEquals(021, TaskCh02N013.solution(120));
    }
}