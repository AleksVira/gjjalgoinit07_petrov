package com.getjavajob.training.algo.init.petrova;

import java.util.Scanner;
import static java.lang.Math.sqrt;

public class TaskCh01N017p {
    public static void main(String[] args) {
        InputNumbers inputNumbers = consoleInput();
        double result = solution(inputNumbers);
        consoleOutput(result);
    }

    private static double solution(InputNumbers inNumbers) {
        double expression = 1 / sqrt((inNumbers.a * inNumbers.x * inNumbers.x + inNumbers.b * inNumbers.x + inNumbers.c));
        return expression;
    }

    private static InputNumbers consoleInput() {
        Scanner inputScanner = new Scanner(System.in);
        InputNumbers numbers = new InputNumbers();
        System.out.print("Введите число a --> ");
        numbers.a = inputScanner.nextInt();
        System.out.print("Введите число b --> ");
        numbers.b = inputScanner.nextInt();
        System.out.print("Введите число c --> ");
        numbers.c = inputScanner.nextInt();
        System.out.print("Введите число x --> ");
        numbers.x = inputScanner.nextInt();
        return numbers;
    }

    private static void consoleOutput(double result) {
        System.out.println("Результат: " + result);
    }

    private static class InputNumbers {
        private int a;
        private int b;
        private int c;
        private int x;
    }
}
