package com.getjavajob.training.algo.init.petrova;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class TaskCh02N039Test {

    @Test
    public void testSolution() throws Exception {
        TaskCh02N039.TimeInput testingTime = new TaskCh02N039.TimeInput();
        double expected;
        double actual;

        testingTime.h = 3;
        testingTime.m = 0;
        testingTime.s = 0;
        expected = 90.0;
        actual = TaskCh02N039.solution(testingTime);
        Assert.assertEquals(expected, actual, 0.0001);

        testingTime.h = 18;
        testingTime.m = 0;
        testingTime.s = 0;
        expected = 180.0;
        actual = TaskCh02N039.solution(testingTime);
        Assert.assertEquals(expected, actual, 0.0001);

        testingTime.h = 16;
        testingTime.m = 30;
        testingTime.s = 0;
        expected = 135.0;
        actual = TaskCh02N039.solution(testingTime);
        Assert.assertEquals(expected, actual, 0.0001);
    }
}