package com.getjavajob.training.algo.init.petrova;

import java.util.Scanner;

public class TaskCh02N031 {
    public static void main(String[] args) {
        int input = consoleInput();
        int result = solution(input);
        consoleOutput(result);
    }

    public static int solution(int inputNum) {
        int firstDigit = inputNum / 100;
        int secondDigit = inputNum % 10;
        int thirdDigit = inputNum % 100 / 10;
        int expression = firstDigit * 100 + secondDigit * 10 + thirdDigit;
        return expression;
    }

    private static void consoleOutput(int result) {
        System.out.print("Исходное число равно ");
        System.out.print(result);
    }

    private static int consoleInput() {
        Scanner inputScanner = new Scanner(System.in);
        System.out.print("Введите трехзначное число --> ");
        int originalNumber = inputScanner.nextInt();
        System.out.println("Вы ввели число " + originalNumber);
        return originalNumber;
    }
}
