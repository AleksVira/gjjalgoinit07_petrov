package com.getjavajob.training.algo.init.petrova;

import java.util.Scanner;

public class TaskCh02N043 {
    public static void main(String[] args) throws Exception {
        NumersAB input = consoleInput();
        int result = solution(input);
        consoleOutput(result);
    }

    public static int solution(NumersAB input) throws Exception{
        int expression = (input.a % input.b) * (input.b % input.a);

        // Про более простой способ с возведением в степень знаю, да...

        int result = 0;
        try {
            result = 1 / expression;
        } catch (Exception e) {
            result = 1;
        }
        return result;
    }


    private static NumersAB consoleInput() {
        Scanner inputScanner = new Scanner(System.in);
        NumersAB numbersInput = new NumersAB();
        System.out.print("Введите первое число --> ");
        numbersInput.a = inputScanner.nextInt();
        System.out.print("Введите второе число --> ");
        numbersInput.b = inputScanner.nextInt();
        return numbersInput;
    }

    private static void consoleOutput(int finalResult) {
        System.out.println("Результат: " + finalResult);
    }

    public static class NumersAB {
        public int a;
        public int b;
    }

}
