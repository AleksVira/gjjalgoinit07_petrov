package com.getjavajob.training.algo.init.petrova;

import java.util.Scanner;

import static java.lang.Math.abs;

public class TaskCh01N017s {
    public static void main(String[] args) {
        int input = consoleInput();
        double result = solution(input);
        consoleOutput(result);
    }

    private static double solution(int input) {
        return abs(input) + abs(input + 1);
    }

    private static int consoleInput() {
        Scanner inputScanner = new Scanner(System.in);
        System.out.print("Введите число --> ");
        int getNumber = inputScanner.nextInt();
        System.out.println("Вы ввели число " + getNumber);
        return getNumber;
    }

    private static void consoleOutput(double result) {
        System.out.println("Результат: " + result);
    }
}
