package com.getjavajob.training.algo.init.petrova;

import java.util.Scanner;

public class TaskCh02N039 {
    public static void main(String[] args) {
        TimeInput input = consoleInput();
        double result = solution(input);
        consoleOutput(result);
    }

    public static double solution(TimeInput timeIn) {
        final double SECONDS_IN_DAY = 43200;
        final double FULL_ANGLE = 360;
        timeIn.h = timeIn.h % 12;
        int totalSeconds = timeIn.h * 60 * 60 + timeIn.m * 60 + timeIn.s;
        double expression = (FULL_ANGLE / SECONDS_IN_DAY) * totalSeconds;
        return expression;
    }

    private static TimeInput consoleInput() {
        Scanner inputScanner = new Scanner(System.in);
        TimeInput timeInput = new TimeInput();
        System.out.print("Введите количество часов --> ");
        timeInput.h = inputScanner.nextInt();
        System.out.print("Введите количество минут --> ");
        timeInput.m = inputScanner.nextInt();
        System.out.print("Введите количество секунд --> ");
        timeInput.s = inputScanner.nextInt();
        return timeInput;
    }

    private static void consoleOutput(double result) {
        System.out.println("Отклонение стрелки в градусах: " + result);
    }

    public static class TimeInput {
        public int h;
        public int m;
        public int s;
    }
}
